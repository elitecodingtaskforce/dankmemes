
Data stream
    |
Load balance
    |
App1 - App2 - ...
  \    /     / (Get state from Redis and update state)
   Redis
    | (Update Redis state)
   REST


References:
https://cylix.github.io/cpp_redis/html/classcpp__redis_1_1client.html
