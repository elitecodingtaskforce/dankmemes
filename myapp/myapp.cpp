
#include <cpp_redis/cpp_redis>
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

cpp_redis::client client;

void store_value(const char *key, const char *value) {

    client.hset("UDP_PACKETS", key, value);

    std::vector<std::pair<std::string,std::string>> recPair;
    client.hgetall("UDP_PACKETS", [&recPair] (cpp_redis::reply & records) {
        for (unsigned int i = 0; i < records.as_array().size(); i+=2) {
            std::cout << "rec num[" << i << "]: " << records.as_array()[i] << " -> " << records.as_array()[i+1] << std::endl;

            std::pair<std::string,std::string> record;
            record.first = records.as_array()[i].as_string();
            record.second = records.as_array()[i + 1].as_string();
            recPair.push_back(record);
        }
    });
    /*client.set(key, value);
    client.get(key, [&key](cpp_redis::reply& reply) {
      std::cout << "Value of: " << key << ": " << reply << std::endl;
    });*/

    client.sync_commit();
}
#define BUFLEN 512
#define NPACK 10
#define PORT 9931

int main() {
    const char *host = "127.0.0.1";

    client.connect(host, 6379, [](const std::string& host, std::size_t port, cpp_redis::client::connect_state status) {
        if (status == cpp_redis::client::connect_state::dropped) {
            std::cout << "client disconnected from " << host << ":" << port << std::endl;
        }
    });

    struct sockaddr_in si_me, si_other;
    int s, i, slen=sizeof(si_other);
    char buf[BUFLEN];

    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1) {
        std::cout << "Error creating socket\n";
    }

    memset((char *) &si_me, 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(PORT);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(s, (const sockaddr*)&si_me, sizeof(si_me))==-1) {
        std::cout << "Error binding\n";
    }

    for (i=0; i<NPACK; i++) {
      if (recvfrom(s, buf, BUFLEN, 0, (sockaddr*)&si_other, (socklen_t*)&slen)==-1) {
        std::cout << "Error recvfrom\n";
      }
      std::cout << "Received packet from " << inet_ntoa(si_other.sin_addr) << ":" << ntohs(si_other.sin_port) << " buf: " << buf;

      std::ostringstream oss;
      oss << inet_ntoa(si_other.sin_addr) << ":" << ntohs(si_other.sin_port);

      std::string key = oss.str();

      store_value(key.c_str(), buf);
    }

    return 0;
}
