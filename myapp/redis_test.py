import time
from redis import StrictRedis

redis = StrictRedis(host='localhost', port=6379)

pubsub = redis.pubsub()
pubsub.psubscribe('__keyspace@0__:UDP_*')

print('Starting message loop')
while True:
    message = pubsub.get_message()
    if message:
        print(message)
        key = message['channel'].split(b':')[1]
        print(redis.hgetall(key))
    else:
        time.sleep(0.01)
