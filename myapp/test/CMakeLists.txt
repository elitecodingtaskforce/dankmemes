
enable_testing()
find_package(GTest REQUIRED)

add_executable(footest footest.cpp)
target_link_libraries(footest GTest::GTest GTest::Main)

add_test(AllTestsInFoo footest)

